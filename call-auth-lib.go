package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-protos-go/peer"
	lib "gitlab.com/eng-siena-ri/ten/ten-identities/tools/tenid-authlib"

	//"gitlab.com/eng-siena-ri/ten/ten-identities/main/tenid-chaincode"

	//lib "gitlab.com/inlecom/interq/tenid-authlib"
	"strings"
)

type example struct{}

func (t *example) Init(stub shim.ChaincodeStubInterface) peer.Response {
	fmt.Println("Inside Init")

	//err := lib.Init(nil, "mychannel")
	err := lib.Init("mychannel")
	if err != nil {
		return shim.Error("ss")
	}

	result := "Init called succesfully"
	return shim.Success([]byte(result))
}

func (t *example) Invoke(stub shim.ChaincodeStubInterface) peer.Response {

	fn, args := stub.GetFunctionAndParameters()
	if fn == "Authentication" {
		fmt.Println("I am going to authentication")
		return t.Authentication(stub, args)
	}

	result := "Ok"
	return shim.Success([]byte(result))
}

func (t *example) Authentication(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	addr, err := lib.Auth(stub)

	if err != nil {
		lib.Log(lib.ERROR, "Authz in ERROR: '", err.Error()+"'")

		if strings.Contains(err.Error(), "TenIdChallengeResponse") {
			var resp = lib.Response{}
			error := json.Unmarshal([]byte(err.Error()), &resp)
			if error != nil {
				lib.Log(lib.ERROR, "tenTokens Authz jsonUnmarshal Response err not nil", error.Error())
			}
			if 401 == resp.Code {
				lib.Log(lib.INFO, "Challenge Response required", resp.Description)
			}
		}
		lib.Log(lib.ERROR, " Authz in Error: '", err.Error()+"'")
		//return fmt.Sprintf("Error : %v ", err)
		//return shim.Error(err.Error())
		return shim.Success([]byte(err.Error()))

		//return ctx.GetStub().
	}
	//	lib.Log(INFO, "Welcome address : ", addr)
	fmt.Println("Welcome address : ", addr)

	//	result := "OK"
	//return shim.Success([]byte(result))
	return shim.Success([]byte(ValResponse(201, "tenIdentities postIdentity putState is OK", string(addr))))
}

func main() {
	err := shim.Start(new(example))
	if err != nil {
		fmt.Printf("Error starting chaincode: %s", err)
	}
}
