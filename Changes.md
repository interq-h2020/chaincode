# Changes 

In tenid-authlib.go 

```azure
. "gitlab.com/eng-siena-ri/challenge-queue-lib"

    instead of 

. "gitlab.com/eng-siena-ri/challenge-lib"

```

In tenid-authlib documentation 
```azure
Log() 
    instead of 
lib.Log()
```

can we have some explicit examples of the new usage of tenid-authlib?