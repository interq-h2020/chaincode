package tenid_authlib

/*
Engineering Ingegneria Informatica SpA licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

/* created by Antonio Scatoloni at 17/09/2020 */
const AUTH_LIB = "tenid-authlib - "
const (
	ERROR_NUMBER_OF_ARGUMENTS          = AUTH_LIB + "Incorrect number of arguments."
	ERROR_UNMARSHALL_IDENTITY_RESPONSE = AUTH_LIB + "error into Unmarshal Identity of response of protectIdentityChaincode"
	ERROR_UNMARSHALL_DOMAIN_RESPONSE   = AUTH_LIB + "error into Unmarshal Domain of response of protectIdentityChaincode"
	ERROR_AUTHENTICATION_FAILED        = AUTH_LIB + "error: authentication failed"
	ERROR_INVALID_IDENTIY              = AUTH_LIB + "error: invalid identity (invalid addr)"
	ERROR_INVALID_SIGNATURE            = AUTH_LIB + "error: invalid signature (invalid sign)"
	ERROR_CHALLENGE_QUEUE              = AUTH_LIB + "Errors encountered creating the ChallengeQueue"
	ERROR_CHANNEL_NAME_EMPTY           = AUTH_LIB + "error the Channel Name is empty"
)
