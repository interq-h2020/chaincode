package tenid_authlib

/*
Engineering Ingegneria Informatica SpA licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"math/big"
	"strconv"
	"time"
)

// Log System Manual Logger
func Log(activity Activity, message string, parameter interface{}) {
	if activity.Code >= LogLevel.Code {
		fmt.Println(time.Now().Format(time.RFC3339)+" "+activity.Description+" MESSAGE: "+message, parameter)
	}
}

// hexToPublicKey creates a public key ecdsa from x and y parameters
func hexToPublicKey(ecdsaSign ECDSApublicKeyXY) *ecdsa.PublicKey {
	xBytes, _ := hex.DecodeString(ecdsaSign.X)
	x := new(big.Int)
	x.SetBytes(xBytes)

	yBytes, _ := hex.DecodeString(ecdsaSign.Y)
	y := new(big.Int)
	y.SetBytes(yBytes)

	pub := new(ecdsa.PublicKey)
	pub.X = x
	pub.Y = y

	pub.Curve = elliptic.P256()

	return pub
}

// hexToRSAPublicKey creates a public key rsa from e and n parameters
func hexToRSAPublicKey(rsaPK RSApublicKeyEN) (*rsa.PublicKey, error) {
	nBytes, _ := hex.DecodeString(rsaPK.N)
	n := new(big.Int)
	n.SetBytes(nBytes)

	e, err := strconv.Atoi(rsaPK.E)
	if err != nil {
		return nil, err
	}

	pub := new(rsa.PublicKey)
	pub.E = e
	pub.N = n

	return pub, nil
}

// validateEcdsaSignature validate a pkeyblob signed with the public key relative at the signature
func validateEcdsaSignature(pbKey *ecdsa.PublicKey, text string, sign Signature) bool {
	hash := sha256.Sum256([]byte(text))
	var esigR = new(big.Int)
	var esigS = new(big.Int)
	var ok bool
	esigR, ok = esigR.SetString(sign.R, 0)
	esigS, ok = esigS.SetString(sign.S, 0)
	if !ok {
		Log(ERROR, "IdentityChaincode PostIdentity Error in signature: ", sign.R+" "+sign.S)
		return ok
	}
	// validate the public key with the signature
	ok = ecdsa.Verify(pbKey, hash[:], esigR, esigS)
	if !ok {
		Log(ERROR, AUTH_LIB+"validateEcdsaSignature error: controller authentication failed", "")
		Log(ERROR, AUTH_LIB+"validateEcdsaSignature Validate of Signature - pbKey   : ", pbKey.X.String()+" "+pbKey.Y.String())
		Log(ERROR, AUTH_LIB+"validateEcdsaSignature Validate of Signature - pKeyBlob: ", text)
		Log(ERROR, AUTH_LIB+"validateEcdsaSignature Validate of Signature - sign.R  : ", sign.R)
		Log(ERROR, AUTH_LIB+"validateEcdsaSignature Validate of Signature - sign.S  : ", sign.S)
	}
	return ok
}

// validateRsaSignature validate a pkeyblob signed with the public key relative at the signature
func validateRsaSignature(pbKey *rsa.PublicKey, text string, sign string) (bool, error) {
	hash := sha256.Sum256([]byte(text))
	// validate the public key with the signature
	err := rsa.VerifyPKCS1v15(pbKey, crypto.SHA256, hash[:], []byte(sign))
	if err != nil {
		Log(ERROR, AUTH_LIB+"validateRsaSignature error: controller authentication failed", "")
		Log(ERROR, AUTH_LIB+"validateRsaSignature Validate of Signature - pbKey    : ", strconv.Itoa(pbKey.E)+" "+pbKey.N.String())
		Log(ERROR, AUTH_LIB+"validateRsaSignature Validate of Signature - pKeyBlob : ", text)
		Log(ERROR, AUTH_LIB+"validateRsaSignature Validate of Signature - signature: ", sign)
		return false, err
	}
	return true, nil
}

// ToChaincodeArgs converts string args to []byte args
func ToChaincodeArgs(args ...string) [][]byte {
	bargs := make([][]byte, len(args))
	for i, arg := range args {
		bargs[i] = []byte(arg)
	}
	return bargs
}
