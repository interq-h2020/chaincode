package tenid_authlib

/*
Engineering Ingegneria Informatica SpA licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

// Aim : Account Identity Manifest
type Aim struct {
	AimBase   `json:"aimBase"`
	AimStatus `json:"aimStatus"`
}

// AimBase : First part of Aim - Account Identity Manifest Base
type AimBase struct {
	Id    string `json:"id"`
	PKey  string `json:"pKey"`
	KType string `json:"kType"`
	End   string `json:"end"`
	Ext   string `json:"ext"`
}

// AimStatus : AimStatus second part of Aim - Account Identity Manifest Status
type AimStatus struct {
	Id     string `json:"id"`
	Start  string `json:"start"`
	Status int    `json:"status"` // 1: ACTIVE, 2: SUSPENDED, 3: REVOKED
}

// ECDSApublicKeyXY : point of ellictic curve that is the same at public key ECDSA
type ECDSApublicKeyXY struct {
	X string `json:"x"`
	Y string `json:"y"`
}

// RSApublicKeyEN : numbers for public key of rsa type
type RSApublicKeyEN struct {
	E string `json:"e"`
	N string `json:"n"`
}

// Domain : Domain for methods of chaincode
type Domain struct {
	Address string `json:"address"` // (PK)
	Parent  string `json:"parent"`
	Owner   string `json:"owner"`
}

// Signature : signature of new publickey of new identity
type Signature struct {
	R string `json:"r"`
	S string `json:"s"`
}

type ChaincodeResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type Activity struct {
	Description string
	Code        int
}

var ChannelName string
var IdentityChaincodeName string
var RoleChaincodeName string
var LogLevel Activity = DEBUG
