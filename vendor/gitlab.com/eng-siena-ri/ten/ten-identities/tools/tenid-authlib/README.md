# tenid-authlib

**tenid-authlib** is a GoLang library which allows to a generic Smart Contract for Hyperledger Fabric (AKA Chaincode) to use _TEn Authentication Mechanism_ based on Challenge Response.
This library exposes two methods.

* `Auth(APIstub shim.ChaincodeStubInterface) (string, error)` (returns the Address of identity authenticated)
*  `Init(channelName string) error` (**optional usage** only if you DON'T USE the HLF default channel `mychannel`)

## Configuration
Import the library in your Chaincode with this code.
```
import (
	lib "gitlab.com/eng-siena-ri/ten/ten-identities/tools/tenid-authlib"
)
```

## Usage
The Auth method is the authentication method, it needs only the HLF shim.ChaincodeStubInterface as parameter.
```
func (s *GreetChaincode) Invoke(APIstub shim.ChaincodeStubInterface) pb.Response {
	Log(DEBUG, "Calling invoke...", "")
	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()

	addr, err := lib.Auth(APIstub) 
	if err != nil {
		Log(ERROR, "Auth in ERROR: '", err.Error()+"'")
		return shim.Error(err.Error())
	}
	Log(INFO, "Welcome address", addr)
	// Put here the logic for your operations
	if function == "hello" {
		return s.hello(APIstub, args)
	} else if function == "bye" {
		return s.bye(APIstub, args)
	}
	return shim.Error("Invalid Smart Contract function name.")
}
```
## License <a name="license"></a>

**tenid-authlib** Project source code files are made available under the Apache License, Version 2.0 (Apache-2.0), located in the [LICENSE](LICENSE) file.