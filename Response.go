package main

import "encoding/json"

// Response : Code Response
type Response struct {
	Header  `json:"header"`
	Message string `json:"message"`
}

// StringResp method for stringify the Json data
func StringResp(resp Response) string {

	var respByte []byte
	var err error
	respByte, err = json.Marshal(resp)
	if err != nil {
		return "500 - Error in Marshal of Response "
	}
	return string(respByte)
}

// ValResp method for stringify the Json data with input parameters
func ValResp(code int, desc string) string {
	var resp Response = Response{}
	resp.Code = code
	resp.Header.Description = desc
	resp.Message = ""
	return StringResp(resp)
}

// ValResp method for stringify the Json data with input parameters
func ValResponse(code int, desc string, mess string) string {
	var resp Response = Response{}
	resp.Code = code
	resp.Header.Description = desc
	resp.Message = mess
	return StringResp(resp)
}
